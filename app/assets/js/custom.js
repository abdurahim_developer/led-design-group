// function openModal() {
//   document.getElementById("myModal").style.display = "block";
// }
//
// function closeModal() {
//   document.getElementById("myModal").style.display = "none";
// }
//
// var slideIndex = 1;
// showSlides(slideIndex);
//
// function plusSlides(n) {
//   showSlides(slideIndex += n);
// }
//
// function currentSlide(n) {
//   showSlides(slideIndex = n);
// }
//
// function showSlides(n) {
//   var i;
//   var slides = document.getElementsByClassName("mySlides");
//   var dots = document.getElementsByClassName("demo");
//   var captionText = document.getElementById("caption");
//   if (n > slides.length) {slideIndex = 1}
//   if (n < 1) {slideIndex = slides.length}
//   for (i = 0; i < slides.length; i++) {
//       slides[i].style.display = "none";
//   }
//   for (i = 0; i < dots.length; i++) {
//       dots[i].className = dots[i].className.replace(" active", "");
//   }
//   slides[slideIndex-1].style.display = "block";
//   dots[slideIndex-1].className += " active";
//   captionText.innerHTML = dots[slideIndex-1].alt;
// }

var owl = $('.owl-product');
owl.owlCarousel({
    items:1,
    loop:true,
    margin:10,
    nav: true,
    dots: false,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true
});

// $('.owl-product').owlCarousel({
//     loop:true,
//     margin:10,
//     dots: false,
//     autoplay:false,
//     autoplayTimeout:5000,
//     autoplayHoverPause:false,
//     nav:true,
//     responsive:{
//         0:{
//             items:1
//         },
//         600:{
//             items:1
//         },
//         1000:{
//             items:1
//         }
//     }
// })