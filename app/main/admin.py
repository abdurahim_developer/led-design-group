from django.contrib import admin

from . import models
# Register your models here.


@admin.register(models.Advantage)
class AdvantageAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ['id','title', 'created']
    list_display_links = ['title']


@admin.register(models.Service)
class ServiceAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ['id', 'title', 'created']
    list_display_links = ['title']


@admin.register(models.Process)
class ProcessAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ['id','title', 'created']
    list_display_links = ['title']


@admin.register(models.Portfolio)
class PortfolioAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ['id', 'title', 'created']
    list_display_links = ['title']


@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id','name', 'slug']
    list_display_links = ['name']
    prepopulated_fields = {"slug": ("name",)}


class SaleImageInline(admin.TabularInline):
    model = models.SaleImage


@admin.register(models.Sale)
class SaleAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ['id', 'title', 'category', 'created']
    list_display_links = ['title']
    inlines = [SaleImageInline]


@admin.register(models.Arenda)
class ArendaAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ['id', 'title', 'created']
    list_display_links = ['title']


@admin.register(models.Award)
class AwardAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ['id', 'title']
    list_display_links = ['id', 'title']


@admin.register(models.Team)
class TeamAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ['id', 'title']
    list_display_links = ['id', 'title']


@admin.register(models.SiteSettings)
class SiteSettingsAdmin(admin.ModelAdmin):
    list_display = ['id']
    list_display_links = ['id']
