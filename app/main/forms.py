from django import forms

class ContactForm(forms.Form):
    """ CONTACT FORM """
    name = forms.CharField(label='', max_length=225, widget=forms.TextInput(attrs={
        'type': 'text',
        'placeholder': 'Name ',
        'class': 'form-control',
    }))

    email = forms.CharField(label='', max_length=255, widget=forms.TextInput(attrs={
        'type': 'email',
        'placeholder': 'Email',
        'class': 'form-control',
    }))
    phone = forms.CharField(label='', max_length=255, widget=forms.TextInput(attrs={
        'type': 'text',
        'placeholder': 'Phone',
        'class': 'form-control',
    }))
    message = forms.CharField(required=True, widget=forms.Textarea(attrs={
        'placeholder': 'Message',
        'class': 'form-control',
    }))
