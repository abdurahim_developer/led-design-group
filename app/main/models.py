from django.db import models

from ckeditor.fields import RichTextField
# Create your models here.


class Advantage(models.Model):
    """ Advantege MODEL """
    title = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField(upload_to='uploads/icon/%Y/%m/%d')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Преимущество'
        verbose_name_plural = 'Преимущество'


class Service(models.Model):
    """ SERVICE MODEL """
    title = models.CharField(max_length=255)
    description = RichTextField(blank=True, null=True)
    image = models.ImageField(upload_to='uploads/services/%Y/%m/%d')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Сервис'
        verbose_name_plural = 'Сервис'


class Process(models.Model):
    """ PROCESS MODEL """
    title = models.CharField(max_length=255)
    description = RichTextField(blank=True, null=True)
    image = models.ImageField(upload_to='uploads/uslugi', null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Услуги'
        verbose_name_plural = 'Наши Услуги'


class Portfolio(models.Model):
    """ PORTFOLIO MODEL """
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    image = models.ImageField(upload_to='uploads/portfolio/%Y/%m/%d')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Портфолио'
        verbose_name_plural = 'Портфолио'


class Category(models.Model):
    """ CATEGORY MODEL """
    name = models.CharField(max_length=255)
    slug = models.SlugField()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категория'


class Sale(models.Model):
    """ SALE MODEL """
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = RichTextField(blank=True, null=True)
    content = RichTextField(blank=True, null=True)
    image = models.ImageField(upload_to='uploads/sale/%Y/%m/%d')
    image_inner = models.ImageField(upload_to='uploads/sale/%Y/%m/%d', null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Продажа'
        verbose_name_plural = 'Продажа'


class SaleImage(models.Model):
    """ SALE IMAGE MODEL """
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE, related_name='slider_image')
    image = models.ImageField(upload_to='uploads/sale/%Y/%m/%d')


class Arenda(models.Model):
    """ ARENDA MODEL """
    title = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField(upload_to='uploads/arenda/%Y/%m/%d')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Аренда'
        verbose_name_plural = 'Аренда'


class Award(models.Model):
    """ Award MODEL """
    title = models.CharField(max_length=255, blank=True)
    image = models.ImageField(upload_to='uploads/awards/%Y/%m/%d')

    def __str__(self):
        return self.title


class Team(models.Model):
    title = models.CharField(max_length=255, blank=True)
    description = RichTextField(blank=True, null=True)
    image = models.ImageField(upload_to='uploads/team/%Y/%m/%d')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class SiteSettings(models.Model):
    """ SITE SETTINGS MODEL """
    address = models.CharField(max_length=255, blank=True)
    content = models.TextField(blank=True, null=True)

    tel_first = models.CharField(max_length=255, blank=True, null=True)
    tel_second = models.CharField(max_length=255, blank=True, null=True)
    fax = models.CharField(max_length=255, blank= True, null=True)
    email = models.EmailField(blank=True, null=True)

    facebook = models.URLField(max_length=500, blank=True, null=True)
    instagram = models.URLField(max_length=500, blank=True, null=True)
    telegram = models.URLField(max_length=500, blank=True, null=True)
    twitter = models.URLField(max_length=500, blank=True, null=True)
    youtube = models.URLField(max_length=500, blank=True, null=True)

    about_page_partners_count = models.CharField(max_length=255, null=True)
    about_page_successfull_projects = models.CharField(max_length=255, null=True)
    about_page_year = models.CharField(max_length=255, null=True)

    def __str__(self):
        return str(self.id)
