from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.HomeView.as_view(), name='home_url'),
    path('about/', views.AboutView.as_view(), name='about_url'),
    path('our-team/', views.TeamView.as_view(), name='team_url'),
    path('awards/', views.AwardsView.as_view(), name='awards_url'),
    path('arenda/', views.ArendaView.as_view(), name='arenda_url'),
    path('sale/', views.SaleView.as_view(), name='sale_url'),
    path('sale/<slug:category_slug>/', views.SaleByCategoryView.as_view(), name='sale_list_by_category'),
    path('portfolio/', views.PortfolioView.as_view(), name='portfolio_url'),
    path('service/', views.ServiceView.as_view(), name='service_url'),
    path('contact/', views.ContactView.as_view(), name='contact_url'),
    path('sale/<int:pk>', views.SaleDetailView.as_view(), name='sale_detail_url'),
]