from django.shortcuts import render
from django.views import generic
from django.core.mail import send_mail
from django.contrib import messages
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from project import settings as project_settings, context

from . import models
from  . import forms
# Create your views here.


class HomeView(generic.TemplateView):
    template_name = 'pages/home/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['advantage'] = models.Advantage.objects.all()
        context['process'] = models.Process.objects.all()
        context['portfolies'] = models.Portfolio.objects.all().order_by('id')
        return context


class AboutView(generic.TemplateView):
    template_name = 'pages/about/about.html'


class TeamView(generic.ListView):
    queryset = models.Team.objects.all()
    template_name = 'pages/about/team.html'
    context_object_name = 'teams'


class AwardsView(generic.ListView):
    queryset = models.Award.objects.all()
    template_name = 'pages/about/awards.html'
    context_object_name = 'awards'


class ArendaView(generic.ListView):
    queryset = models.Arenda.objects.all().order_by('id')
    template_name = 'pages/arenda.html'
    context_object_name = 'arendas'
    # paginate_by = 2


class SaleView(generic.ListView):
    queryset = models.Sale.objects.all().order_by('id')
    template_name = 'pages/sale.html'
    context_object_name = 'products'


class SaleDetailView(generic.DeleteView):
    template_name = 'pages/sale_datail.html'
    model = models.Sale
    context_object_name = 'sale'


class SaleByCategoryView(generic.ListView):
    queryset = models.Sale.objects.all().order_by('id')
    template_name = 'pages/sale_by_category.html'
    context_object_name = 'products'

    def get_queryset(self):
        return self.queryset.filter(category__slug=self.kwargs.get('category_slug'))


class PortfolioView(generic.ListView):
    queryset = models.Portfolio.objects.all().order_by('id')
    template_name = 'pages/portfolio.html'
    context_object_name = 'portfolies'
    # paginate_by = 20


class ServiceView(generic.ListView):
    queryset = models.Service.objects.all().order_by('id')
    template_name = 'pages/service.html'
    context_object_name = 'services'


class ContactView(generic.FormView):
    form_class = forms.ContactForm
    template_name = 'pages/contact.html'

    def get_success_url(self, *args, **kwargs):
        return self.request.path

    def form_valid(self, form):
        name = form.cleaned_data['name']
        email = form.cleaned_data['email']
        phone = form.cleaned_data['phone']
        message = form.cleaned_data['message']
        data = {"message": message, "number": phone, "email": email}

        try:
            is_sent = send_mail(
                "Message from {} from website Led Design Group".format(name),
                "Phone number: {number}\nE-Mail: {email}\nMessage: {message}".format(**data),
                project_settings.EMAIL_HOST_USER,
                [context.defaults(self.request)['settings'].email]
            )
        except Exception as e:
            is_sent = False
            messages.add_message(self.request, messages.ERROR, ("Sorry, we can't receive your request. Please try again later"))
        else:
            messages.add_message(self.request, messages.SUCCESS, ('Your request received'))
        return super(ContactView, self).form_valid(form)
