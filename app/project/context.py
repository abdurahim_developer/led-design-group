from main import models


def defaults(req):
    return {
        "settings": models.SiteSettings.objects.first(),
        "categories": models.Category.objects.all(),
    }
